2D Schrödinger equation simulation
==================================

The ``SimulationManager`` class
*******************************

.. automodule:: quantum_nodes.algorithms.schrodinger_equation.simulation_manager
   :members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance:

The ``SimulationCache`` class
*****************************

.. automodule:: quantum_nodes.algorithms.schrodinger_equation.simulation_cache
   :members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance:

The ``SimulationDataManager`` class
***********************************

.. automodule:: quantum_nodes.algorithms.schrodinger_equation.simulation_data_manager
   :members:
   :undoc-members:
   :show-inheritance:

The ``SimulationInputsManager`` class
*************************************

.. automodule:: quantum_nodes.algorithms.schrodinger_equation.simulation_inputs_manager
   :members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance: