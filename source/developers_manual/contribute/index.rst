.. _contribute-to-quantum-nodes:

Contribute
==========

Here are all the necessary information on how to contribute to Quantum Nodes.


.. _guidelines-contrib:

Guidelines
##########


.. toctree::
    :maxdepth: 1
    :glob:

    guidelines_addon
    guidelines_manual


.. _dev-env-contrib:

Development environment
#######################


.. _ide-dev-env:

IDE
---

We recommend you to use `Visual Studio Code <https://code.visualstudio.com/>`_.

Tools for VSCode:

.. toctree::
    :maxdepth: 1
    :glob:

    tools

Here are more detailed information on how to setup a comfortable development environment
depending on you operating system.

.. toctree::
    :maxdepth: 1
    :glob:

    linux
    windows
    mac

.. _instructions-contrib:

Contribute
##########


.. note::
    Click `here <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_ to learn about the forking workflow on Gitlab.


* Fork our git repositories: `addon <https://gitlab.com/quantum-creative-group/quantum_nodes>`_, `manual <https://gitlab.com/quantum-creative-group/quantum_nodes_manual>`_
* Do your modifications
* Open a new merge request
* Wait for your modifications to be reviewed and accepted


.. _git-workflow-contrib:

Git workflow
############

.. image:: https://miro.medium.com/max/560/1*UH5ozOBwkaFhWA1mrJkVIQ.png
    :alt: Git workflow
    :align: center
    :width: 80%
    :class: img-rounded

|