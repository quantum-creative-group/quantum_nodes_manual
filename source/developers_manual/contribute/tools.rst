.. _tools-contrib:

Tools
=====

Here is a list of tools which will help you to develop.

* :ref:`vs-code-extensions-contrib`
    * :ref:`blender-vscode`
    * :ref:`pydocstring-generator-vscode`
    * :ref:`rst-vscode`
* :ref:`anaconda-with-windows-powershell`

.. _vs-code-extensions-contrib:

VSCode extensions
#################


.. _blender-vscode:

Blender Development
-------------------


.. note::
    Tools to simplify Blender development. Developed by `Jacques Lucke <https://github.com/JacquesLucke>`_.


* Install `blender development <https://marketplace.visualstudio.com/items?itemName=JacquesLucke.blender-development>`_


.. _pydocstring-generator-vscode:

Python Docstring Generator
--------------------------


.. note::
    Automatically generates the right docstring format for methods / functions / classes ...


* Install `python docstring generator <https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring>`_

* Select the ``google`` format for the auto docstring functionality

.. image:: https://gitlab.com/quantum-creative-group/quantum_nodes_manual/-/raw/assets/contrib-tools/docstring_format.png
    :width: 85%
    :alt: Python Docstring Generator, auto docstring google
    :align: center
    :class: img-rounded

|


.. _rst-vscode:

reStructuredText Syntax highlighting
------------------------------------


.. note::
    Syntax highlighting and document symbols for reStructuredText


* Install `reStructuredText syntax highlighting <https://marketplace.visualstudio.com/items?itemName=trond-snekvik.simple-rst>`_
* This extension uses `Esbonio <https://swyddfa.github.io/esbonio/docs/latest/en/>`_
* Select the right output for sphinx-build in the settings:

.. image:: https://gitlab.com/quantum-creative-group/quantum_nodes_manual/-/raw/assets/contrib-tools/esbonio_output_sphinx_build.png
    :width: 85%
    :alt: reStructuredText syntax highlighting, set output path sphinx-build
    :align: center
    :class: img-rounded

|

.. _anaconda-with-windows-powershell:

Anaconda with Windows Powershell
################################

If you are using an Anaconda environment (or miniconda) on Windows, you may want to use conda commands with Powershell.
Here is one way to enable it (source: `stackoverflow <https://stackoverflow.com/questions/64149680/how-to-activate-conda-environment-from-powershell>`_)

* Open anaconda prompt and type: ``conda init powershell``
* Open powershell in **administrator mode** and type: ``Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted``

At this point, you should be able to use conda commands in powershell.

If you want to enable it in the integrated terminal of VSCode, you can add these lines to your configuration:

.. code-block:: JSON

    "terminal.integrated.defaultProfile.windows": "Windows PowerShell",
    "terminal.integrated.profiles.windows": {
        "PowerShell": {
            "source": "PowerShell",
            "args": [
                "C:\\Users\\<username>\\<miniconda3 or anaconda3>\\shell\\condabin\\conda-hook.ps1"
            ],
            "icon": "terminal-powershell"
        }
    },
