.. _linux-dev-env:

Linux
=====

.. _blender-linux-dev-env:

Blender
#######

Download a version of Blender from the `releases page <https://download.blender.org/release/>`_ instead of installing it globally.
Thus you will be able to manage several versions of Blender independently.

* Download a version of Blender
* Untar the downloaded file where you want (common destination: ``/opt``)


.. _dependencies-linux-dev-env:

Dependencies
############


.. _dependencies-an-linux-dev-env:

Animation Nodes
---------------

Following this step will enable syntax highlighting and auto-completion when using classes and functions from
the animation_nodes module.

* Download source code of Animation Nodes from their `Github repo <https://github.com/JacquesLucke/animation_nodes>`_.
* Extract the *animation_nodes* folder from the downloaded file next to the *quantum_nodes* folder.
* Add the path where the *animation_nodes* folder is located in the PYTHONPATH variable.
  
    * One way is to add these lines at the end of ~/.bashrc:
      
            .. code-block:: bash

                # Quantum Nodes configuration
                export PYTHONPATH="path/to/the/folder:$PYTHONPATH"

    * Example:

            .. code-block:: text

                I have a folder named 'QuantumNodes' located at /home/felix/Documents.
                It contains:
                * quantum_nodes
                * quantum_nodes_manual
                * animation_nodes

                So I will add these lines at the end of my file:

                # Quantum Nodes configuration
                export PYTHONPATH="/home/felix/Documents/QuantumNodes:$PYTHONPATH"


.. _dependencies-python-packages-linux-dev-env:

Python packages
---------------


.. important::
    We recommend you to setup an anaconda environment and link it to Blender.


* If using an anaconda environment:
    * Install: ``sudo pip install -r requirements.txt``

* If not using an anaconda environment:
    * Install: ``sudo ./python3.X -m pip install -r requirements.txt -t ../lib/site-packages``


    .. note::
        If it does not work, you can follow the instructions in the installation guide for Linux (:ref:`install-dependencies-linux`).