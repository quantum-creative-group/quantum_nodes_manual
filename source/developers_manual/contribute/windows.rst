.. _windows-dev-env:

Windows
=======


.. _blender-windows-dev-env:

Blender
#######

We recommend you to download a portable (.zip file) version of Blender from the `releases page <https://download.blender.org/release/>`_
instead of installing it globally. Thus you will be able to manage several versions of Blender independently.

* Download a version of Blender
* Unzip the downloaded file where you want (**you should have all the rights in the destination folder**)


.. _dependencies-windows-dev-env:

Dependencies
############


.. _dependencies-an-windows-dev-env:

Animation Nodes
---------------


* Download source code of Animation Nodes from their `Github repo <https://github.com/JacquesLucke/animation_nodes>`_.
* Extract the *animation_nodes* folder from the downloaded file next to the *quantum_nodes* folder.
* Add the path where the *animation_nodes* folder is located in the PYTHONPATH variable.
  
    * One way is to add the path at the end of the PYTHONPATH environment variable.
      If the variable does not exist, create it.

    * Example:

            .. code-block:: text

                I have a folder named 'QuantumNodes' located at C:\Users\felix\Documents.
                It contains:
                * quantum_nodes
                * quantum_nodes_manual
                * animation_nodes

                So I will add this to the PYTHONPATH variable:
                C:\Users\felix\Documents\QuantumNodes

.. image:: https://gitlab.com/quantum-creative-group/quantum_nodes_manual/-/raw/assets/contrib-tools/environment-variable-windows-contrib-addon.png
    :width: 85%
    :alt: Environment variable windows, add path to PYTHONPATH
    :align: center
    :class: img-rounded

|


.. _dependencies-python-packages-windows-dev-env:

Python packages
---------------

* Go where python is installed in the Blender files (``blender/x.x/python/bin``)
* Install: ``python.exe -m pip install -r [path/to/quantum_nodes]/requirements.txt --upgrade --no-cache-dir -t ../lib/site-packages``

.. note::
    If it does not work, you can follow the instructions in the installation guide for Windows (:ref:`install-dependencies-windows`).